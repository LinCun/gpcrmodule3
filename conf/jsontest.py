import json
from docking import templates

config = {
			"executable" : None,
			"envVariables" : None,
			"modellerDir" : ["/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2/models"],
			"templateDir" : ["/home/maiage/tplnguyen/Documents/GPCR/testmodule2/templates"],
			"templateLibrary" : {} #{
			}

def conf():
	global config
	modelDir = config["modellerDir"]
	tempDir = config["templateDir"]
	tempLib = templates.templateLib(modelDir[0], tempDir[0])
	for temp, oripath in tempLib:
		config["templateLibrary"][temp] = oripath
	return config

def confMake(confFile):
	print "Configuration file is being written into " + confFile
	f = open(confFile, "w")
	json.dump(conf(), f, indent=4, sort_keys=True)

if __name__ == "__main__":
	#print(conf())
	#print config
	confMake("/home/maiage/tplnguyen/Documents/GPCR/venv/modules/module3/module/conf/confModule3.json")
					
