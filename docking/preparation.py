#from __future__ import with_statement
import sys

"""
Prepare ligand, receptor and parameters files.
ligand: ligand filename
receptor: receptor filename
scriptFile: output script filename
*OutPath: output filename
"""

aut = "/usr/local/genome/mgltools-1.5.6/MGLToolsPckgs"

def vinaConf(ligand, recep, path, **kwargs):
	"""
	Write configuration file for autodock vina.
	"""
	npts = "24.75,24.75,24.75"
	gridcenter = "61.583,16.239,13.194"
	
	if kwargs is not None:
		if 'npts' in kwargs:
			npts = kwargs['npts']
			x = npts.split(",")[0]
			x = float(x) * 0.375
			npts = "%s,%s,%s" % (x,x,x)
		if 'gridcenter' in kwargs:
			gridcenter = kwargs['gridcenter']

	conf = "receptor = %s \n" % recep
	conf += "ligand = %s \n\n" % ligand
	conf += "center_x = %s \n" % gridcenter.split(",")[0]
	conf += "center_y = %s \n" % gridcenter.split(",")[1]
	conf += "center_z = %s \n\n" % gridcenter.split(",")[2]
	conf += "size_x = %s \n" % npts.split(",")[0]
	conf += "size_y = %s \n" % npts.split(",")[1]
	conf += "size_z = %s \n\n" % npts.split(",")[2]
	conf += "num_modes = 10\n"

	confFile = ligand.split("/")[-1].replace(".pdbqt", "") + "_" + recep.split("/")[-1].replace(".pdbqt", "") + "_conf.txt"
	confOutPath = path + "/" + confFile 

	f = open(confOutPath, "w")
	f.write(conf)
	return confFile

def gpfDump(ligand, recep, scriptFile, gpfOutPath, **kwargs):
	"""
	Write script file for preparing GPF file
	"""
	npts = "66,66,66"
	gridcenter = "61.583,16.239,13.194"
	
	if kwargs is not None:
		if 'npts' in kwargs:
			npts = kwargs['npts']
		if 'gridcenter' in kwargs:
			gridcenter = kwargs['gridcenter']
	
	string = "#!/bin/bash \n"
	string += "pythonsh " + aut + "/AutoDockTools/Utilities24/prepare_gpf4.py -l " + ligand + " -r " + recep
	string += " -p npts=" + npts + " -p gridcenter='" + gridcenter + "' -o " + gpfOutPath + "> gpfMake.log"

	f = open(scriptFile, "w")
	f.write(string)
		
def dpfDump(ligand, recep, scriptFile, dpfOutPath):
	"""
	Write script file for preparing DPF file
	"""
	string = "#!/bin/bash \n"
	string += "pythonsh " + aut + "/AutoDockTools/Utilities24/prepare_dpf4.py -l " + ligand + " -r " + recep + " -o " + dpfOutPath
	
	f = open(scriptFile, "w")
	f.write(string)

def ligandDump(ligand, scriptFile, ligandOutPath):
	"""
	Write script file for preparing ligand file
	"""
	string = "#!/bin/bash \n"
	string += "pythonsh " + aut + "/AutoDockTools/Utilities24/prepare_ligand4.py -l " + ligand + " -o " + ligandOutPath

	f = open(scriptFile, "w")
	f.write(string)

def recepDump(recep, scriptFile, recepOutPath):
	"""
	Write script file for preparing receptor file
	"""
	string = "#!/bin/bash \n"
	string += "pythonsh " + aut + "/AutoDockTools/Utilities24/prepare_receptor4.py -r " + recep + " -o " + recepOutPath

	f = open(scriptFile, "w")
	f.write(string)

