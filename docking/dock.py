import os
import sys
from docking import preparation
from services import utils


def makeGPF(ligand, recep, workDir, **kwargs):
	"""
	Write script file for making GPF file
	"""
	scriptFile = workDir + "/GPFMake.sh"
	gpfOutPath = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "_" + recep.split("/")[-1].replace(".pdbqt", "") + ".gpf"
	script = preparation.gpfDump(ligand, recep, scriptFile, gpfOutPath, **kwargs)
	utils.chmodX(scriptFile)
	return gpfOutPath

def makeDPF(ligand, recep, workDir):
	"""
	Write script file for making DPF file
	"""
	scriptFile = workDir + "/DPFMake.sh"
	dpfOutPath = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "_" + recep.split("/")[-1].replace(".pdbqt", "") + ".dpf"
	script = preparation.dpfDump(ligand, recep, scriptFile, dpfOutPath)
	utils.chmodX(scriptFile)
	return dpfOutPath
	
def autogrid4(gpfFile, path):
	"""
	Write script file for running autogrid4
	"""
	scriptFile = path + "/autogrid4Run.sh"
	gpfOutFile = ".".join(gpfFile.split(".")[:-1])
	gpfOutFile = ".".join([gpfOutFile, "glg"])
	string = "#!/bin/bash \n"
	string += "cd %s \n" % path
	string += "autogrid4 -p %s -l %s" % (gpfFile, gpfOutFile)
	f = open(scriptFile, "w")
	f.write(string)
	return scriptFile

def autodock4(dpfFile, path):
	"""
	Write script file for running autodock4
	"""
	scriptFile = path + "/autodock4Run.sh"
	dpfOutFile = ".".join(dpfFile.split(".")[:-1])
	dpfOutFile = ".".join([dpfOutFile, "dlg"])
	string = "#!/bin/bash \n"
	string += "cd %s \n" % path
	string += "autodock4 -p %s -l %s" % (dpfFile, dpfOutFile)
	f = open(scriptFile, "w")
	f.write(string)
	return scriptFile

def vina(ligand, recep, conf, path):
	"""
	Write script file for running autodock vina
	"""
	scriptFile = path + "/vinaRun.sh"
	log = ligand.split("/")[-1].replace(".pdbqt", "") + "_" + recep.split("/")[-1].replace(".pdbqt", "") + ".log"
	output = ligand.split("/")[-1].replace(".pdbqt", "") + "_" + recep.split("/")[-1].replace(".pdbqt", "") + ".pdbqt"
	string = "#!/bin/bash \n"
	string += "cd %s \n" % path
	string += "vina --config %s --log %s --out %s" % (conf, log, output)
	f = open(scriptFile, "w")
	f.write(string)
	return scriptFile

def dock4(ligand, recep, workDir, **kwargs):
	"""
	Write script file for processing docking with autodock4
	"""
	"""if not os.path.isfile(ligand) or not os.path.isfile(recep):
		print "Ligand or receptor not found"
		sys.exit(0)
	# GPF
	print "Creating grid parameters file . . .\n"
	gpf = makeGPF(ligand, recep, workDir, **kwargs)
	# DPF
	print "Creating dock parameters file . . .\n"
	dpf = makeDPF(ligand, recep, workDir)
	# Run autogrid4
	print "Running autogrid4 . . .\n"
	atg4 = autogrid4(gpf, workDir)
	utils.chmodX(atg4)
	os.system(atg4)
	# Run autodock4
	print "Running autodock4, please wait . . .\n"
	atd4 = autodock4(dpf, workDir)
	utils.chmodX(atd4)
	os.system(atd4)
	return atg4, atd4"""
	if not os.path.isfile(ligand) or not os.path.isfile(recep):
		print "Ligand or receptor not found"
		sys.exit(0)

	scriptFile = workDir + "/dock4.sh"

	string = "#!/bin/bash \n"
	string += "cd %s \n" % workDir
	string += "printf \"Creating grid parameters file . . .\"\n"
	gpf = makeGPF(ligand, recep, workDir, **kwargs)
	string += "./GPFMake.sh\n"
	string += "printf \"Creating dock parameters file . . .\"\n"
	dpf = makeDPF(ligand, recep, workDir)
	string += "./DPFMake.sh\n"
	string += "printf \"Running autogrid4 . . .\"\n"
	atg4 = autogrid4(gpf, workDir)
	utils.chmodX(atg4)
	string += "./autogrid4Run.sh\n"
	string += "printf \"Running autodock4, please wait . . .\"\n"
	atd4 = autodock4(dpf, workDir)
	utils.chmodX(atd4)
	string += "./autodock4Run.sh\n"
	string += "printf 'Docking done !'"

	f = open(scriptFile, "w")
	f.write(string)
	return scriptFile

def dockVina(ligand, recep, workDir, **kwargs):
	"""
	Write script file for processing docking with autodock vina
	"""
	"""if not os.path.isfile(ligand) or not os.path.isfile(recep):
		print "Ligand or receptor not found"
		sys.exit(0)

	print "Creating configuration file . . .\n"
	conf = preparation.vinaConf(ligand, recep, workDir, **kwargs)
	
	print "Running autodock vina . . .\n"
	vn = vina(ligand, recep, conf, workDir)
	utils.chmodX(vn)
	os.system(vn)"""
	if not os.path.isfile(ligand) or not os.path.isfile(recep):
		print "Ligand or receptor not found"
		sys.exit(0)
	
	conf = preparation.vinaConf(ligand, recep, workDir, **kwargs)
	scriptFile = workDir + "/dockVina.sh"
	
	string = "#!/bin/bash \n"
	string += "cd %s \n" % workDir
	vn = vina(ligand, recep, conf, workDir)
	utils.chmodX(vn)
	string += "./vinaRun.sh\n"

	f = open(scriptFile, "w")
	f.write(string)
	return scriptFile

