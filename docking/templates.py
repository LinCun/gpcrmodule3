import os
import sys
from services import utils
import json
import re

"""
Modeller version 9.16
"""

templateLibrary = []

def parseModelLog(logFile):
	"""
	Read modeller log file and return the 2 best DOPE score models name
	"""
	bestmodels = []
	# Open log file
	f = open(logFile, "r")
	file = f.read()
	#print file
	# Find summary table
	patternStart = ">> Summary of successfully produced models:"
	pattern = re.compile(r"^%s(?:.*\n){3}((.+\n)+)"%(patternStart), re.MULTILINE)
	modelsScore = []
	m = re.search(pattern, file)
	if m:
		modelsScore = m.groups()[0].split("\n")
	# Find 2 best score models
	modelMap = {}
	for model in modelsScore:
		if model:
			model = model.split()
			modelMap[model[0]] = model[1:]
	sortedMap = sorted(modelMap.items(), key=lambda x: x[1][1])
	#print modelMap
	#print sortedMap[-1][0]
	bestmodels.append(sortedMap[-1][0])
	bestmodels.append(sortedMap[-2][0])
		
	return bestmodels

def parseModelName(modelFile):
	"""
	Read a model file to assign name to that model.
	"""
	# Read file
	f = open(modelFile, "r")
	file = f.readlines()
	# Extract model number
	no = modelFile.split("/")[-1]
	no = no.split(".")[-2][-2:]
	# Extract query sequence name and template name
	seq, temp = "", ""
	for line in file:
		#print line		
		patternSeq = re.search(r"SEQUENCE:(.+)", line)
		patternTemp = re.search(r"TEMPLATE:(.+)", line)
		if patternSeq:
			#print line
			seq = patternSeq.groups()[0]
			seq = seq.strip().lower()			
		if patternTemp:
			temp = patternTemp.groups()[0]
			temp = temp.strip().lower()
	modelname = seq + "_" + temp + "_" + no
	return modelname

def loadTemplateLibrary(bean):
	global templateLibrary
	if os.path.isdir(bean):
		templateLibrary = bean["templates"]

def templateLib(modelDir, tempDir):
	"""
	Supposed that all modeller jobs are executed in a same working directory:
		modelDir: modeller working directory
		tempDir: output directory which is the templates library
	This function aims to find modeller jobs in modelDir and then copy the 2 best models into tempDir.
	"""
	#templateLib
	# Check available of templates directory and create it if not exist
	if not os.path.isdir(tempDir):
		print "Make sure that you do not give wrong path"
		os.makedirs(tempDir)
	# Check available of modeller working directory, raise error if not exist
	if not os.path.isdir(modelDir):
		raise IOError('Modeller working directory does not exist: %s' % modelDir)
	# Find modeller folders inside modeller wdir
	modelPathList = utils.find_path(modelDir, "doModel.log")
	for path in modelPathList:
		logFile = path + "/doModel.log"
		# Best models
		bestModels = parseModelLog(logFile)
		for model in bestModels:
			# Create templates folder
			mname = parseModelName(path + "/" + model)

			global templateLibrary
			if mname not in templateLibrary:
				templateLibrary.append([mname, path + "/" + model])

			tempFolder = "_".join(mname.split("_")[:-2])
			tempFolder = tempDir + "/" + tempFolder
			if not os.path.isdir(tempFolder):
				os.makedirs(tempFolder)
			# Copy file 
			utils.cp(path + "/" + model, tempFolder, fileOut=mname + ".pdb")
	return templateLibrary

"""class Template(object):
	def __init__(self, **kwargs):
		self.model = None
		self.path = None
		self.id = None
		self.folder = None
		self.modelBeanBase = {
							"comments": None,
							"modelDir": None,
							"registeredModel": None
						  }
		self.modelBean = {
							"comments": None,
							"id": None,
							"pdbFolder": None
							}
		if kwargs is not None:
			if 'modelDir' in kwargs:
				self.modelDir = kwargs['modelDir']
			if 'registeredModel' in kwargs:
				self.registeredModel = kwargs['registeredModel']
			if 'path' in kwargs:
				# path of input
				self.path = kwargs['path']
			\"""if not self.path:
				print 'No path provided'
				sys.exit(0)\"""
	def modelsBase(self, modelDir):
		if not os.path.isdir(modelDir):
			print "Make sure that you do not give wrong path"
			os.mkdir(modelDir)
		modelPathList = utils.find_path(self.path, "*.pdb")
		for path in modelPathList:
			newPath = "_".join(path.split("/")[-2:])
			newPath = "/".join([modelDir, newPath])
			if not os.path.isdir(newPath): os.mkdir(newPath)
			#print newPath
			modelList = utils.find_file(path, "*.pdb")
			#print modelList
			for m in modelList:
				pathIn = "/".join([path, m])
				#print pathIn
				utils.cp(pathIn, newPath)
	def updateLib(self, pathIn, pathOut):
		\"""
		Writing bean file for all model available in a directory.
		(For example: 3P0G_I103L)
		\"""
		if not os.path.isdir(pathIn):
			raise NameError
		modelPathList = utils.find_path(pathIn, "*.pdb")
		for path in modelPathList:
			self.modelBean['pdbFolder'] = path
			beanFile = "_".join(path.split("/")[-2:])
			self.id = beanFile
			beanPath = "/".join([pathOut, beanFile])
			if not os.path.isdir(pathOut): os.mkdir(pathOut)
			#print newPath
			modelList = utils.find_file(path, "*.pdb")
			self.modelBeanDump(beanPath)
			#print modelList
	def updateLib2(self, pathIn, pathOut):
		\"""
		Writing bean file for all model available in module1 outdir.
		\"""
		if not os.path.isdir(pathIn):
			raise IOError		
		modelPathList = utils.find_path(pathIn, "*.pdb")
		for path in modelPathList:
			self.modelBean['pdbFolder'] = path
			if utils.find_file(path, "doModel.py"):
				beanFile = utils.doModelParse(path + "/doModel.py")[0]
				self.id = beanFile
				beanPath = "/".join([pathOut, beanFile])
				if not os.path.isdir(pathOut): os.mkdir(pathOut)
				#print newPath
				self.modelBeanDump(beanPath)				
				
	def modelBeanDump(self, beanPath):
		\"""
		Writing bean file.
		\"""
		self.modelBean['id'] = self.id
		self.modelBean['comments'] = "auto-generated modeller's models bean file"
		if beanPath:
			with open(beanPath, 'w') as p:
				json.dump(self.modelBean, p)
	#def allModelBean(self):"""
		

if __name__ == '__main__':
	"""pathIn = '/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2'
	pathOut = '/home/maiage/tplnguyen/Documents/TestPyDock/Bean2'
	tem = Template()
	#pathIn.modelsBase(pathOut)
	tem.updateLib2(pathIn, pathOut)"""
	"""logFile = "/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2/models/hhAlign_0/doModel.log"
	a = parseModelLog(logFile)
	print a"""
	"""modelFile = "/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2/models/hhAlign_0/OR1G1_HUMAN.B99990005.pdb"
	print(parseModelName(modelFile))"""
	modelpath = "/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2/models"
	tempdir = "/home/maiage/tplnguyen/Documents/GPCR/testmodule2/templates2"
	print(templateLib(modelpath, tempdir))
			
		
