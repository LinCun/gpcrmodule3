import os
import sys
import stat
from fnmatch import fnmatch


def chmodX(filePath):
	"""
	Change file permissions.
	Argument:
		filePath: path + filename.
    """
	st = os.stat(filePath)
	os.chmod(filePath, st.st_mode | stat.S_IEXEC)


def find_path(path, pattern):
	"""
	Return a list of directories that contain files that match a pattern.
	Arguments:
		path: the directory that you want to looking for the files inside.
		pattern: the pattern of filename that you want to find, e.g: "*.pdb"
	Return only the parent directory, not the filenames.
	"""
	if not os.path.isdir(path):
		print "Error: cannot find", path
		sys.exit(0)
	subfoldersList = []
	for folder, subfolders, files in os.walk(path):
		for file in files:
			if fnmatch(file, pattern):
				filepath = os.path.join(folder, file)
				filepath = "/".join(filepath.split("/")[:-1])
				if filepath not in subfoldersList:
					subfoldersList.append(filepath)
			
	return subfoldersList

def find_file(path, pattern):
	"""
	Return a list of files whose name matchs a pattern.
	Arguments:
		path: the directory that you want to looking for the files inside.
		pattern: the pattern of filename that you want to find, e.g: "*.pdb"
	Return only the list of filenames, not included in their path.
	"""
	if not os.path.isdir(path):
		print "Error: cannot find ", path
		sys.exit(0)
	filesList = []
	for file in os.listdir(path):
		#print file
		if os.path.isfile(os.path.join(path, file)) and fnmatch(file, pattern):
			if file not in filesList:
				filesList.append(file)
	return filesList

def cp(fileIn, path, fileOut=None):
	"""
	Copy a file.
	Arguments:
		fileIn: path + filename of the input file.
		path: path of output.
		fileOut: output filename. If not available, input filename is used. Default None.
	Return output path + filename.
	"""
	if fileOut:
		fileOut = path + "/" + fileOut
	else:
	#fileOut = "/".join([path,fileIn.split("/")[-1]])
		fileOut = path + "/" + fileIn.split("/")[-1]
	#open(fileOut, "w").write(open(fileIn).read())
	f = open(fileIn).read()
	fo = open(fileOut, "w")
	fo.write(f)
	return fileOut

def delChars(string, chars):
	"""
	Delete all characters defined by chars from a string.
	Arguments:
		string: target string.
		chars: a list of characters that need to be deleted from string.
	Return the output string.
	"""
	for c in chars:
		string = string.replace(c, "")
	return string

def doModelParse(file):
	"""
	Parse doModel.py file to get template and query name.
	"""
	parse = []
	f = open(file, 'r')
	for line in f:
		if fnmatch(line, "*knowns*=*"):
			template = line.split()[2]
			template = delChars(template, "'\",")
			#print template
		if fnmatch(line, "*sequence*=*"):		
			query = line.split()[2]
			query = delChars(query, "'\",")
			model = "_".join([query, template])
			parse=[model, [query, template]]
	return parse

if __name__ == '__main__':
	path = "/home/maiage/tplnguyen/Documents/Modeller/Active/3P0G/"
	pattern = "*.pdb"
	#print find_file(path, pattern)
	#print os.listdir(subfs(path, pattern)[0])
	path2 = '/home/maiage/tplnguyen/Documents/GPCR/test_usage_module_1/Threading2/models/hhAlign_0/doModel.py'
	print doModelParse(path2)
