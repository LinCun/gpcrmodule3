import sys, getopt
from docking import molecule
from docking import dock
import os
from services import utils
import json
import drmaa

ligand = None
selectedTemplates = []
templateLib = {}
beanConfig = {}
env = os.environ.copy()
adt4 = False
vina = False


def loadLigand(ligandInputFile, workDir):
	"""
	If ligand file format is PDB: prepare ligand by MGL and place output in workDir.
	If ligand file format is already PDBQT: copy file into workDir.
	Arguments:
		ligandInputFile: path + filename.
		workDir: working directory that will be parent directory for docking.
	"""
	global ligand
	if ligandInputFile.endswith("pdbqt"):
		ligand = ligandInputFile
		utils.cp(ligand, workDir)
	if ligandInputFile.endswith("pdb"):
		ligand = molecule.Molecule(ligandInputFile)
		ligand.makeLigand(workDir)

def loadTemplates(workDir):
	"""
	If --selectedTemplates defined: prepare each templates in selected list by MGL and place in separated folder (workDir/ligandname_receptorname).
	If --selectedTemplates not defined: prepare each templates in library by MGL and place in separated folder (workDir/ligandname_receptorname).
	"""
	global templateLib
	global beanConfig
	global selectedTemplates
	global vina, adt4

	if vina:
		dockType = "vina"
	elif adt4:
		dockType = "adt4"

	if "templateDir" not in beanConfig:
		raise ValueError, "No templateDir in " + str(beanConfig)

	if selectedTemplates:
		for tempDir in beanConfig["templateDir"]:
			for temp in selectedTemplates:
				template = utils.find_path(tempDir, temp)[0] + "/" + temp + ".pdb" 
				if template:
					template = molecule.Molecule(template)
					path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/" + dockType
					if not os.path.exists(path): os.makedirs(path)
					print "Loading receptor " + temp + " to " + path + " . . . \n"
					template.makeReceptor(path)
	else:
		for temp in beanConfig["templateLibrary"]:
			for tempDir in beanConfig["templateDir"]:
				template = utils.find_path(tempDir, temp)[0] + "/" + temp + ".pdb"
				if template:
					template = molecule.Molecule(template)
					path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/" + dockType
					if not os.path.exists(path): os.makedirs(path)
					print "Loading receptor " + temp + " to " + path + " . . . \n"
					template.makeReceptor(path)

def docking4(workDir, **kwargs):
	global ligand
	global selectedTemplates

	loadTemplates(workDir) 	

	if selectedTemplates:
	
		# Start shared drmaa session for all jobs in pipeline
		drmaa_session = drmaa.Session()
		drmaa_session.initialize()
		print("\n-------> Docking drmaa session starting <-------\n")

		jobListId = []
		job_temp = drmaa_session.createJobTemplate()

		for temp in selectedTemplates:
			path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/adt4"
			#if not os.path.exists(path): os.makedirs(path) 				
			print "Loading ligand query file to " + path + " . . . \n"
			loadLigand(ligand, path)
			ligand = path + "/" + ligand.split("/")[-1]
			recep = path + "/" + temp + ".pdbqt"
			d = dock.dock4(ligand, recep, path, **kwargs)
			utils.chmodX(d)
			job_temp.workingDirectory = path
			job_temp.joinFiles = True
			job_temp.nativeSpecification= "-b n"
			job_temp.outputPath = ":" + path
			job_temp.errorPath = ":" + path
			job_temp.jobEnvironment = {"PATH": os.environ["PATH"], 'PYTHONPATH': os.environ['PYTHONPATH']}
			job_temp.remoteCommand = d
			jobListId.append(drmaa_session.runJob(job_temp))

		for i, curentJob in enumerate(jobListId):
			print "Collecting job " + curentJob + " . . . "
			retval = drmaa_session.wait(curentJob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
			if retval.hasExited:
				print "Job: {0} finished with status {1}\n".format(retval.jobId, retval.hasExited)
			elif retval.wasAborted:
				print "Job: %s never ran" % (retval.jobId)
				if retval.hasCoreDump:
					print "Job dumped core"
				else:
					print "Job crashed"
			"""if retval.hasSignaled():
				print "Job: %s was signalled with %s" % (retval.jobId, retval.terminatingSignal)"""

		drmaa_session.deleteJobTemplate(job_temp)
		drmaa_session.exit()

	else:
		
		# Start shared drmaa session for all jobs in pipeline
		drmaa_session = drmaa.Session()
		drmaa_session.initialize()
		print("\n-------> Docking drmaa session starting <-------\n")

		jobListId = []
		job_temp = drmaa_session.createJobTemplate()

		for temp in beanConfig["templateLibrary"]:
			path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/adt4"
			#if not os.path.exists(path): os.makedirs(path) 				
			print "Loading ligand query file to " + path + " . . . \n"
			loadLigand(ligand, path)
			ligand = path + "/" + ligand.split("/")[-1]
			recep = path + "/" + temp + ".pdbqt"
			d = dock.dock4(ligand, recep, path, **kwargs)
			utils.chmodX(d)
			job_temp.workingDirectory = path
			job_temp.joinFiles = True
			job_temp.nativeSpecification= "-b n"
			job_temp.outputPath = ":" + path
			job_temp.errorPath = ":" + path
			job_temp.jobEnvironment = {"PATH": os.environ["PATH"], 'PYTHONPATH': os.environ['PYTHONPATH']}
			job_temp.remoteCommand = d
			jobListId.append(drmaa_session.runJob(job_temp))

		for i, curentJob in enumerate(jobListId):
			print "Collecting job " + curentJob + " . . . "
			retval = drmaa_session.wait(curentJob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
			if retval.hasExited:
				print "Job: {0} finished with status {1}\n".format(retval.jobId, retval.hasExited)
			elif retval.wasAborted:
				print "Job: %s never ran" % (retval.jobId)
				if retval.hasCoreDump:
					print "Job dumped core"
				else:
					print "Job crashed"
			"""if retval.hasSignaled():
				print "Job: %s was signalled with %s" % (retval.jobId, retval.terminatingSignal)"""

		drmaa_session.deleteJobTemplate(job_temp)
		drmaa_session.exit()

def dockingVina(workDir, **kwargs):
	global ligand
	global selectedTemplates

	loadTemplates(workDir)
	
	if selectedTemplates:

		# Start shared drmaa session for all jobs in pipeline
		drmaa_session = drmaa.Session()
		drmaa_session.initialize()
		print("\n-------> Docking drmaa session starting <-------\n")

		jobListId = []
		job_temp = drmaa_session.createJobTemplate()

		for temp in selectedTemplates:
			path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/vina"
			print "Loading ligand query file to " + path + " . . . \n"
			loadLigand(ligand, path)
			ligand = path + "/" + ligand.split("/")[-1]
			recep = path + "/" + temp + ".pdbqt"
			d = dock.dockVina(ligand, recep, path, **kwargs)
			utils.chmodX(d)
			job_temp.workingDirectory = path
			job_temp.joinFiles = True
			job_temp.nativeSpecification= "-b n"
			job_temp.outputPath = ":" + path
			job_temp.errorPath = ":" + path
			job_temp.jobEnvironment = {"PATH": os.environ["PATH"], 'PYTHONPATH': os.environ['PYTHONPATH']}
			job_temp.remoteCommand = d
			jobListId.append(drmaa_session.runJob(job_temp))

		for i, curentJob in enumerate(jobListId):
			print "Collecting job " + curentJob + " . . . "
			retval = drmaa_session.wait(curentJob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
			if retval.hasExited:
				print "Job: {0} finished with status {1}\n".format(retval.jobId, retval.hasExited)
			elif retval.wasAborted:
				print "Job: %s never ran" % (retval.jobId)
				if retval.hasCoreDump:
					print "Job dumped core"
				else:
					print "Job crashed"
			"""if retval.hasSignaled():
				print "Job: %s was signalled with %s" % (retval.jobId, retval.terminatingSignal)"""

		drmaa_session.deleteJobTemplate(job_temp)
		drmaa_session.exit()
	else:

		# Start shared drmaa session for all jobs in pipeline
		drmaa_session = drmaa.Session()
		drmaa_session.initialize()
		print("\n-------> Docking drmaa session starting <-------\n")

		jobListId = []
		job_temp = drmaa_session.createJobTemplate()

		for temp in beanConfig["templateLibrary"]:
			path = workDir + "/" + ligand.split("/")[-1].replace(".pdbqt", "") + "/" + temp + "/vina"				
			print "Loading ligand query file to " + path + " . . . \n"
			loadLigand(ligand, path)
			ligand = path + "/" + ligand.split("/")[-1]
			recep = path + "/" + temp + ".pdbqt"
			d = dock.dockVina(ligand, recep, path, **kwargs)
			utils.chmodX(d)
			job_temp.workingDirectory = path
			job_temp.joinFiles = True
			job_temp.nativeSpecification= "-b n"
			job_temp.outputPath = ":" + path
			job_temp.errorPath = ":" + path
			job_temp.jobEnvironment = {"PATH": os.environ["PATH"], 'PYTHONPATH': os.environ['PYTHONPATH']}
			job_temp.remoteCommand = d
			jobListId.append(drmaa_session.runJob(job_temp))

		for i, curentJob in enumerate(jobListId):
			print "Collecting job " + curentJob + " . . ."
			retval = drmaa_session.wait(curentJob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
			if retval.hasExited:
				print "Job: {0} finished with status {1}\n".format(retval.jobId, retval.hasExited)
			elif retval.wasAborted:
				print "Job: %s never ran" % (retval.jobId)
				if retval.hasCoreDump:
					print "Job dumped core"
				else:
					print "Job crashed"
			"""if retval.hasSignaled():
				print "Job: %s was signalled with %s" % (retval.jobId, retval.terminatingSignal)"""

		drmaa_session.deleteJobTemplate(job_temp)
		drmaa_session.exit()
				
def usage():
	"""
	Set help documentary.
	"""
	print "module3.1 -l <ligand filename> --workDir <Output directory> --selectedTemplates=<template names>\n"
	print """
Options:\n
	-c			Config path + filename\n
	--gridcenter		Grid parameter: gridcenter: default "61.583,16.239,13.194"\n
	-h			Print usage and exit\n
	-l			Ligand path + filename\n
	--npts			Grid parameter: npts: default "66,66,66"\n
	--selectedTemplates	List of selected templates (seperated by ","). If not, all templates will be docked\n
	--workDir		Working directory (results directory)
	--adt4		Run autodock4
	--vina		Run autodock vina"""
	sys.exit(0)
				
def readConfig(path):
	"""
	Read configuration file.
	"""
	global beanConfig
	print "Reading configuration from " + path + " . . . \n"
	f = open(path)
	beanConfig = json.load(f)
	"""if "envVariables" in beanConfig:
		for nVar in beanConfig["envVariables"]:
			env[nVar] = beanConfig["envVariables"][nVar]
	if bTemplateView:
		print str(templateLib)"""

def main(argv):
	"""
	Extract options and execute them.
	"""
	beanConfigPath = None
	npts = None
	gridcenter = None
	workDir = os.getcwd()
	
	global selectedTemplates
	global ligand
	global vina, adt4
	
	try:
		opts, args = getopt.getopt(argv,"hi:l:c:", ["workDir=", "selectedTemplates=", "npts=", "gridcenter=", "vina", "adt4"])
	except getopt.GetoptError as e:
		print e
		usage()
		sys.exit(22)
	for opt, arg in opts:
		if opt == "-h":
			usage()
		elif opt == "-l":
			ligand = arg
			print ligand
		elif opt == "--workDir":
			if os.path.exists(arg):
				workDir = arg
			else:
				workDir = os.getcwd() + "/" + arg
				os.mkdir(workDir)
		elif opt == "--selectedTemplates":
			selectedTemplates = arg.split(',')
		elif opt ==  "-c":
			beanConfigPath = arg
			print beanConfigPath
		elif opt == "--gridcenter":
			gridcenter = arg
		elif opt == "--npts":
			npts = arg
		elif opt == "--adt4":
			adt4 = True
		elif opt == "--vina":
			vina = True
		else:
			print "Unkwnown argument " + opt
			sys.exit()

	if beanConfigPath:
		readConfig(beanConfigPath)
	
	if adt4:
		if npts:
			docking4(workDir, npts = npts)
		elif gridcenter:
			docking4(workDir, gridcenter = gridcenter)
		else:
			docking4(workDir)
	if vina:
		if npts:
			dockingVina(workDir, npts = npts)
		elif gridcenter:
			dockingVina(workDir, gridcenter = gridcenter)
		else:
			dockingVina(workDir)

if __name__ == "__main__":
    main(sys.argv[1:])
    
    
    #python module3.1.py -l "/home/maiage/tplnguyen/Desktop/code/code2/testcode2/6m5h2o.pdbqt" -c "/home/maiage/tplnguyen/Documents/PyDock/module3/confModule2.json" --workDir "/projet/mig/work/tplnguyen/testmodule3" --selectedTemplates "or1g1_human_1u19a_03" --vina
	#python module3.py -l "/home/maiage/tplnguyen/Desktop/code/code2/testcode2/6m5h2o.pdbqt" -c "/home/maiage/tplnguyen/Documents/PyDock/module3/confModule2.json" --workDir "/projet/mig/work/tplnguyen/testmodule3" --selectedTemplates "or1g1_human_1u19a_03,or1g1_human_1u19a_08" --adt4

