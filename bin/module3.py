import sys, getopt
from docking import molecule
from docking import dock
import os
from services import utils
import json
import drmaa

ligand = None
selectedTemplates = []
templateLib = {}
beanConfig = {}
env = os.environ.copy()


def loadLigand(ligandInputFile, workDir):
	"""
	If ligand file format is PDB: prepare ligand by MGL and place output in workDir.
	If ligand file format is already PDBQT: copy file into workDir.
	Arguments:
		ligandInputFile: path + filename.
		workDir: working directory that will be parent directory for docking.
	"""
	global ligand
	if ligandInputFile.endswith("pdbqt"):
		ligand = ligandInputFile
		utils.cp(ligand, workDir)
	if ligandInputFile.endswith("pdb"):
		ligand = molecule.Molecule(ligandInputFile)
		ligand.makeLigand(workDir)

def loadTemplates(workDir):
	"""
	If --selectedTemplates defined: prepare each templates in selected list by MGL and place in separated folder (workDir/ligandname_receptorname).
	If --selectedTemplates not defined: prepare each templates in library by MGL and place in separated folder (workDir/ligandname_receptorname).
	"""
	global templateLib
	global beanConfig
	global selectedTemplates
	if "templateDir" not in beanConfig:
		raise ValueError, "No templateDir in " + str(beanConfig)
	if selectedTemplates:
		for tempDir in beanConfig["templateDir"]:
			for temp in selectedTemplates:
				#print tempDir, temp
				#print utils.find_path(tempDir, temp)
				template = utils.find_path(tempDir, temp)[0] + "/" + temp + ".pdb"
				if template:
					print template
					template = molecule.Molecule(template)
					path = workDir + "/" + temp
					#print path
					print "Loading receptor " + temp + " to " + path
					if not os.path.exists(path): os.makedirs(path) 
					template.makeReceptor(path)
	else:
		for temp in beanConfig["templateLibrary"]:
			for tempDir in beanConfig["templateDir"]:
				template = utils.find_path(tempDir, temp)[0] + "/" + temp + ".pdb"
				if template:
					template = molecule.Molecule(template)
					path = workDir + "/" + temp
					print "Loading receptor " + temp + " to " + path
					if not os.path.exists(path): os.makedirs(path) 
					template.makeReceptor(path)

def docking(workDir, **kwargs):
	"""
	Process docking.
	Arguments:
		workDir: working directory.
		**kwargs: in case "npts" and "gridcenter" are set.
	"""
	global ligand
	global selectedTemplates
	
	if selectedTemplates:

		# Start shared drmaa session for all jobs in pipeline
		drmaa_session = drmaa.Session()
		drmaa_session.initialize()
		print("Docking drmaa session starting...")

		jobListId = []
		job_temp = drmaa_session.createJobTemplate()

		for temp in selectedTemplates:
			#print temp
			path = workDir + "/" + temp
			ligand = utils.cp(workDir + "/" + ligand.split("/")[-1], path)
			recep = path + "/" + temp + ".pdbqt"
			d = dock.dock4(ligand, recep, path, **kwargs)
			#print atg4, atd4
			#print job_temp.WORKING_DIRECTORY
			job_temp.workingDirectory = path
			
			#job_temp.WORKING_DIRECTORY = path
			job_temp.joinFiles = True
			job_temp.nativeSpecification= "-b n"
			job_temp.outputPath = ":" + path
			job_temp.errorPath = ":" + path
			job_temp.jobEnvironment = {"PATH": os.environ["PATH"], 'PYTHONPATH': os.environ['PYTHONPATH']}
			#script = path + "/DPFMake.sh"
			#utils.chmodX(script)
			job_temp.remoteCommand = d
			#job_temp.args = ['-c', "print('hello from python!')"]
			#job_temp.remoteCommand = atd4
			jobListId.append(drmaa_session.runJob(job_temp))
			#job_temp.remoteCommand = atd4
			#jobListId.append(drmaa_session.runJob(job_temp))
			#print "Docking of ligand " + ligand.split("/")[-1].replace(".pdbqt", "") + " on receptor " + temp

		for i, curentJob in enumerate(jobListId):
			print "Collecting job " + curentJob
			retval = drmaa_session.wait(curentJob, drmaa.Session.TIMEOUT_WAIT_FOREVER)
			#print drmaa_session.jobStatus(curentJob)
			if retval.hasExited:
				print "Job: {0} finished with status {1}".format(retval.jobId, retval.hasExited)
			elif retval.wasAborted:
				print "Job: %s never ran" % (retval.jobId)
				if retval.hasCoreDump:
					print "Job dumped core"
				else:
					print "Job crashed"
			"""if retval.hasSignaled():
				print "Job: %s was signalled with %s" % (retval.jobId, retval.terminatingSignal)"""

		drmaa_session.deleteJobTemplate(job_temp)
		drmaa_session.exit()
	else:
		for temp in beanConfig["templateLibrary"]:
			path = workDir + "/" + temp
			recep = path + "/" + temp + ".pdbqt"
			dock.dock(ligand, recep, path, **kwargs)
	
	
						
	
			
def usage():
	"""
	Set help documentary.
	"""
	print "module3 -l <ligand filename> --workDir <Output directory> --selectedTemplates=<template names>\n"
	print """
Options:\n
	-c			Config path + filename\n
	--gridcenter		Grid parameter: gridcenter: default "61.583,16.239,13.194"\n
	-h			Print usage and exit\n
	-l			Ligand path + filename\n
	--npts			Grid parameter: npts: default "66,66,66"\n
	--selectedTemplates	List of selected templates (seperated by ","). If not, all templates will be docked\n
	--workDir		Working directory (results directory)"""
	sys.exit(0)
				
def readConfig(path):
	"""
	Read configuration file.
	"""
	global beanConfig
	print "Reading configuration from " + path
	f = open(path)
	beanConfig = json.load(f)
	"""if "envVariables" in beanConfig:
		for nVar in beanConfig["envVariables"]:
			env[nVar] = beanConfig["envVariables"][nVar]
	if bTemplateView:
		print str(templateLib)"""

def main(argv):
	"""
	Extract options and execute them.
	"""
	beanConfigPath = None
	npts = None
	gridcenter = None
	workDir = os.getcwd()
	adt4 = False
	vina = False
	global selectedTemplates
	global ligand
	
	try:
		opts, args = getopt.getopt(argv,"hi:l:c:", ["workDir=", "selectedTemplates=", "npts=", "gridcenter=", "vina", "adt4"])
	except getopt.GetoptError as e:
		print e
		usage()
		sys.exit(22)
	for opt, arg in opts:
		if opt == "-h":
			usage()
		elif opt == "-l":
			ligand = arg
			print ligand
		elif opt == "--workDir":
			#workDir = arg if os.path.exists(arg) else os.getcwd() + "/" + arg, os.mkdir(workDir)
			if os.path.exists(arg):
				workDir = arg
			else:
				workDir = os.getcwd() + "/" + arg
				os.mkdir(workDir)
		elif opt == "--selectedTemplates":
			selectedTemplates = arg.split(',')
		elif opt ==  "-c":
			beanConfigPath = arg
			print beanConfigPath
		elif opt == "--gridcenter":
			gridcenter = arg
		elif opt == "--npts":
			npts = arg
		elif opt == "--adt4":
			adt4 = True
		elif opt == "vina":
			vina = True
		else:
			print "Unkwnown argument " + opt
			sys.exit()

	if beanConfigPath:
		readConfig(beanConfigPath)

	if ligand:
		print "Loading ligand query file to " + workDir
		loadLigand(ligand, workDir)
	
	loadTemplates(workDir)

	if npts:
		docking(workDir, npts = npts)
	elif gridcenter:
		docking(workDir, gridcenter = gridcenter)
	else:
		docking(workDir)

if __name__ == "__main__":
    main(sys.argv[1:])
    
    
    #python module3.py -l "/home/maiage/tplnguyen/Desktop/code/code2/testcode2/6m5h2o.pdbqt" -c "/home/maiage/tplnguyen/Documents/PyDock/module3/confModule2.json" --workDir "/projet/mig/work/tplnguyen/testmodule3" --selectedTemplates "or1g1_human_1u19a_03,or1g1_human_1u19a_08"

